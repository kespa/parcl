# parcl
## PArsing ARguments for the Command Line

A simple, lenient command-line argument parsing library and help text generator for Haxe and its Sys-compatible targets
[Documentation](https://sharpcdf.codeberg.page/parcl/);

```
import parcl.Parcl;

class Main {
    
    static public function main() {
        var quietmode = false;
        var lang: String;

        var cli = new Parcl("PARCL", "MAKE CLIS", "MADE BY SHARPCDF/KESPA");
        cli.addArg(HelpArgument.Flag("quiet", "suppresses the log"));
        cli.addArg(HelpArgument.Option("language", "what language do you use?"));
        cli.addHelpFlags();
        cli.checkResults();
        
        for (arg in cli.results) {
            switch (arg) {
                case ResultArg.Flag(v):
                    if (v.name == "quiet") {
                        quietmode = true;
                    }
                default:
                    continue;
            }
        }
        if (cli.hasName("language") && !quietmode) {
            var lang = cli.value("language");
            Sys.println('using $lang' + (lang.toLowerCase() != "haxe" ? ", maybe use Haxe next time!" : ", good choice!"));
        }
    }
}
```
## Result
```
❯ neko main.n
PARCL
   MAKE CLIS

Flags:
      -quiet                                                                          suppresses the log
      -h                                                                              Displays this help
      -help                                                                           Displays this help

Options:
      --language=STRING                                                               what language do you use?


MADE BY SHARPCDF/KESPA
```
//* note: not actually a discordrpc application lol
import parcl.Parcl;

class Main {
    
    static public function main() {
        var suppress = false;
        var username = "";

        var p = new Parcl("discord rpc", "make custom rpc for discord", "an example by sharpcdf");
        p.addArg(HelpArgument.Flag("quiet", "suppress logs"));
        p.addArg(HelpArgument.Option("username", ""));
        p.addArg(HelpArgument.Normal("start", "starts the bot"));
        p.addHelpFlags();
        p.checkResults(true, "maybe check help (-h)?");

        for (arg in p.results) {
            switch (arg) {
                case Flag(v):
                    if (v.name == "quiet") {
                        suppress = true;
                    }
                case Option(v):
                    if (v.name == "username") {
                        username = v.value;
                        trace('using ${username} as hackerz namez');
                    }
                case Normal(v):
                    if (v.name == "start") {
                        trace("starting bot..");
                    if (p.hasName("username")) {
                            trace("bot started, check your discord presence!");
                        } else {
                            trace("no username was specified, try again :/");
                        }
                    }
                default:
                    break;
            }
        }
    }
}
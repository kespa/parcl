package parcl;

import parcl.Parcl.ResultArg;

//simple command-line help text generator
class Sharcl {
    private var helpArgs = new Array<HelpArgument>();
    private var title: String;
    private var subtitle: String;
    private var footer: String;

    @:allow(Parcl)
    private  function new(title: String, subtitle = "", footer = "") {
        this.helpArgs = new Array<HelpArgument>();

        this.title = title;
        this.subtitle = subtitle;
        this.footer = footer;
    }
    public function addArg(arg: HelpArgument) {
        helpArgs.push(arg);
    }
    public function generate(): String{
        
        var res = '${title}\n   ${subtitle}\n';
        var switches = "\nSwitches:\n";
        var flags = "\nFlags:\n";
        var options = "\nOptions:\n";
        var other = "\nOther:\n";
        
        for (arg in helpArgs) {
            switch (arg) {
                case Switch(n, d):
                    var spaces = "";
                    var name = '--$n:(on|off)';
                    while (name.length + spaces.length < 80) { //makes the descriptions spaced equally
                        spaces += " ";
                    }
                    switches += '      $name$spaces$d\n';
                case Flag(n, d):
                    var spaces = "";
                    var name = '-$n';
                    while (name.length + spaces.length < 80) {
                        spaces += " ";
                    }
                    flags += '      $name$spaces$d\n';
                case Normal(n, d):
                    var spaces = "";
                    while (n.length + spaces.length < 80) {
                        spaces += " ";
                    }
                    other += '      $n$spaces$d\n';
                case Option(n, d):
                    var spaces = "";
                    var name = '--$n=STRING';
                    while (name.length + spaces.length < 80) {
                        spaces += " ";
                    }
                    options += '      $name$spaces$d\n';
            }
        }
        if (flags != "\nFlags:\n") { //checks if the string had any args added to it
            res += flags;
        }
        if (options != "\n:Options:\n") {
            res += options;
        }
        if (switches != "\nSwitches:\n") {
            res += switches;
        }
        if (other != "\nOther:\n") {
            res += other;
        }
        
        res += '\n\n${footer}';
        return res;
    }
}

//parses the raw arg to a usable enum
function parseArg(flag: String): ResultArg {
    if (StringTools.startsWith(flag, "--")) { // compares flag to syntax of switch or option
        if (StringTools.endsWith(flag, ":on") || StringTools.endsWith(flag, ":off")) { // if it matches the switch syntax
            var trimmedflag = StringTools.trim(flag); // trims spaces and leading --
            trimmedflag = StringTools.replace(trimmedflag, "-", "").toLowerCase();
            var split = trimmedflag.split(":"); // splits key:value into key, value
            switch split[1] { // maps string to boolean
                case "on":
                    // switches.set(split[0], true);
                    return Switch({name: split[0], value: true});
                case "off":
                    return Switch({name: split[0], value: true});
            }
        } else if (StringTools.contains(flag, "=")
            && !(StringTools.endsWith(flag, "=") || StringTools.startsWith(flag, "="))) { // if it matches the option syntax
            var trimmedflag = StringTools.trim(flag);
            trimmedflag = StringTools.replace(trimmedflag, "-", "");
            var split = trimmedflag.split("=");
            return Option({name: split[0], value: split[1]});
        } else {
            return Normal({name: flag});
        }
    } else if (StringTools.startsWith(flag, "-")) {
        return Flag({name: StringTools.replace(flag, "-", "")});
    } else {
        return Normal({name: flag});
    }
    return Normal({name: null});
}

enum HelpArgument {
    Switch(name: String, description: String);
    Flag(name: String, description: String);
    Normal(name: String, description: String);
    Option(name: String, description: String);
}

function helpToStruct(r:HelpArgument):{name:String, type:String} {
	switch (r) {
		case Switch(v, _):
			return {name: v, type:"Switch"};
		case Flag(v, _):
			return {name: v, type:"Flag"};
		case Option(v, _):
			return {name: v, type:"Option"};
		case Normal(v, _):
			return {name: v, type:"Normal"};
		default:
			return {name: null, type: null};
    }
}
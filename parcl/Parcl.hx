package parcl;

import parcl.Sharcl.parseArg;
import Sys;
private typedef ArgSwitch = {name:String, value:Bool};
private typedef ArgFlag = {name:String};
private typedef ArgNormal = {name:String};
private typedef ArgOption = {name:String, value:String};
private typedef ArgClass = {name:String, type:String}; // possibly change in the future

// simple arg parsing class
class Parcl extends parcl.Sharcl {
	private var rawflags:Array<String>;

	public var results:Array<parcl.Parcl.ResultArg>;

	public function new(title:String, subtitle = "", footer = "") {
		super(title, subtitle, footer);

		results = new Array<parcl.Parcl.ResultArg>();

		rawflags = Sys.args();

		if (rawflags.length == 0) { // doesnt run if no args are provided to avoid null error
			return;
		}
		for (arg in rawflags) { // parses args
			results.push(parseArg(arg));
		}
	}
//if error is true, the error is printed and the program closes, otherwise you can handle it yourself
//body is what is shown in addition to the error message
//checks if any argument given is defined
	public function checkResults(error = true, body = ""): Bool { 
		for (arg in results) { // checks if undocumented arg was used, errors if true
			var found = false;
			for (harg in helpArgs) {
				var s1 = resultToStruct(arg);
				var s2 = Sharcl.helpToStruct(harg);
				//trace(s1, s2);
				if ((s1.name == s2.name) && s1.type == s2.type) {
					found = true;
					break;
				} else {
					continue;
				}
			}
			if (!found) {
				if (error) {
					errored(RunError(resultToStruct(arg).name, body));
				} else {
					return false;
				}
			}
		}
		return true;
	}
	private function errored(e:ArgError) {
		switch (e) {
			case RunError(arg, body):
				Sys.println('RunError: argument $arg is not documented' + (body != "" ? ':\n\t$body' : ''));
				Sys.exit(1);
			default:
				Sys.println("unknown error occured");
				Sys.exit(1);
		}
	}

	// returns false if specified argument is not provided
	public function hasArg(arg:ResultArg):Bool {
		for (v in this.results) {
			if (v == arg) {
				return true;
			}
		}
		return false;
	}
	//checks for any arg with `name`, useful in situations where `--option=*` presence needs to be confirmed without matching specific values
	public function hasName(name: String): Bool {
		for (v in this.results) {
			var x = resultToStruct(v);
			if (x.name == name) {
				return true;
			}
		}
		return false;
	}
	//gets the value of an arg, returns null if arg does not exist, or if the arg is not an option or switch
	public function value(argname: String): Dynamic {
		for (v in this.results) {
			switch (v) {
				case Switch(v):
					if (v.name == argname) {
						return v.value;
					}
				case Option(v):
					if (v.name == argname) {
						return v.value;
					}
				default:
					continue;
			}
		}
		return null;
	}

	public function addHelpFlags(autosetHelp = true) {
		addArg(HelpArgument.Flag("h", "Displays this help")); // adds the help flags
		addArg(HelpArgument.Flag("help", "Displays this help"));
		if (autosetHelp) {
			setHelp(this.generate());
		}
	}

	// the help text to show if --help or -h is used, if defaultArg is true then it is also run when no arguments are provided
	public function setHelp(text:String, defaultArg = true) {
		var helpTriggered = false;
		for (arg in results) {
			switch (arg) {
				case Flag(v):
					if (v.name == "h" || v.name == "help") {
						helpTriggered = true;
						break;
					}
				default:
					continue;
			}
		}
		if (helpTriggered) {
			Sys.println(text);
			Sys.exit(0);
		} else if (defaultArg && rawflags.length == 0) {
			Sys.println(text);
			Sys.exit(0);
		}
	}
}

function resultToStruct(r:ResultArg):ArgClass {
	switch (r) {
		case Switch(v):
			return {name: v.name, type: "Switch"};
		case Flag(v):
			return {name: v.name, type: "Flag"};
		case Option(v):
			return {name: v.name, type: "Option"};
		case Normal(v):
			return {name: v.name, type: "Normal"};
		default:
			return {name: null, type: null};
	}
}

enum ResultArg {
	Switch(v:ArgSwitch);
	Flag(v:ArgFlag);
	Option(v:ArgOption);
	Normal(v:ArgNormal);
}

enum ArgError {
	RunError(arg:String, body:String);
}
